#ifndef CHESS_H_INCLUDED
#define CHESS_H_INCLUDED

#define ligne 12
#define colonne 10
#define DEPTH 10

typedef enum PieceType PieceType;
enum PieceType
{
  NONE, QUEEN, ROOK, BISHOP, KNIGHT, PAWN, KING
};

typedef enum Color Color;
enum Color
{
  WHITE, BLACK
};

typedef struct Coordonnees Coordonnees;
struct Coordonnees
{
  int x;
  int y;
};

typedef struct Piece Piece;
struct Piece
{
  Color color;
  PieceType pieceType;
  Coordonnees coords;
};

typedef struct Field Field;
struct Field
{
  Piece piece;
};

typedef struct Can_Castle Can_Castle;
struct Can_Castle
{
  int cast1;
  int cast2;
};

typedef struct PiecesList PiecesList;
struct PiecesList
{
    int blackPawns[5], whitePawns[5];
    int totalBlack, totalWhite;
};

typedef struct Position Position;
struct Position
{
  Field board[colonne][ligne];
  Color turn;
  Can_Castle cas_w, cas_b;
  int en_passant;
  Position* prev;
  int irr_change;
  Coordonnees king_w, king_b;
  int number;
  int eval, noteForNow;
};

int identical_position(Position x, Position y);
int draw_by_repetition_aux(Position pos, Position p, int n);
void init();
void drawBoard();
void newGame();
char ccouleur(Field f);
char ctype(Field f);
int int_of_column(char c);
char column_of_int(int n);
PiecesList countPieces(Position pos);


#endif // CHESS_H_INCLUDED
