#ifndef PAWNS_H_INCLUDED
#define PAWNS_H_INCLUDED

#define QUEEN       900
#define ROOK        500
#define BISHOP      325
#define KNIGHT      300
#define PAWN        100

#endif // PAWNS_H_INCLUDED
