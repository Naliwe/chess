#include <stdio.h>
#include <stdlib.h>

#include "chess.h"
#include "rules.h"

int King(Position pos, Coordonnees p, Coordonnees f)
{
    int a = 0;
    if ((f.y==p.y+1 && f.x==p.x) ||(f.y==p.y-1 && f.x==p.x)||(f.y==p.y+1 &&
       f.x==p.x+1)||(f.y==p.y+1 && f.x==p.x-1)||(f.y==p.y-1 && f.x==p.x+1)||(f.y==p.y-1 &&
       f.x==p.x-1)||(f.y==p.y && f.x==p.x+1)||(f.y==p.y && f.x==p.x-1)) {a = 1;}
       return a;
}

int Queen(Position pos, Coordonnees p, Coordonnees f)
{
    if(p.x==f.x||p.y==f.y)
    {
        //if queen moves in + direction
        return Rook(pos, p, f);
    }
    else if(abs(f.x-p.x)==abs(p.y-f.y))
    {
        //if queen moves in diagonal direction
        return Bishop(pos, p, f);
    }
    else
    return 0;
}

int Rook(Position pos, Coordonnees p, Coordonnees f)
{
    int i;
    int a=1;
    if(f.y==p.y)
    {
        for(i=p.x+1;i<f.x;i++)
        {
            if(pos.board[i][f.y+1].piece.pieceType != NONE)
            {
                a = 0;
                break;
            }
        }
        for(i=p.x-1;i>f.x;i--)
        {
            if(pos.board[i][f.y+1].piece.pieceType != NONE)
            {
                a = 0;
                break;
            }
        }
    }
    else if(f.x==p.x)
    {
        for(i=p.y+1;i<f.y;i++)
        {
            if(pos.board[f.x][i+1].piece.pieceType != NONE)
            {
                a = 0;
                break;
            }
        }
        for(i=p.y-1;i>f.y;i--)
        {
            if(pos.board[f.x][i+1].piece.pieceType != NONE)
            {
                a = 0;
                break;
            }
        }
    }
    else
    {
        a=0;
    }
    return a;
}

int Bishop(Position pos, Coordonnees p, Coordonnees f)
{
    int a=1, i;
    if(abs(p.x-f.x)!=abs(p.y-f.y))
        a=0;
    else if((p.x<f.x)&&(p.y<f.y))
    {
        for(i=1;(i+p.x)<f.x;i++)
        {
            if(pos.board[p.x+i][p.y+i+1].piece.pieceType != NONE)
                a=0;
        }
    }
    else if((p.x>f.x)&&(p.y>f.y))
    {
        for(i=1;(p.x-i)>f.x;i++)
        {
            if(pos.board[p.x-i][p.y+1-i].piece.pieceType != NONE)
                a=0;
        }
    }
    else if((p.x>f.x)&&(p.y<f.y))
    {
        for(i=1;(p.x-i)>f.x;i++)
        {
            if(pos.board[p.x+i][p.y+1-i].piece.pieceType != NONE)
                a=0;
        }
    }
    else if((p.x<f.x)&&(p.y>f.y))
    {
        for(i=1;(p.y-i)>f.y;i++)
        {
            if(pos.board[p.x-i][p.y+1+i].piece.pieceType != NONE)
                a=0;
        }
    }
    else
    {
        a=0;
    }
    return a;
}

int Knight(Position pos, Coordonnees p, Coordonnees f)
{
    int a = 0;
    if((f.y==p.y+2 && f.x==p.x+1)||(f.y==p.y+2 && f.x==p.x-1)||(f.y==p.y+1 &&
    f.x==p.x+2)||(f.y==p.y+1 && f.x==p.x-2)||(f.y==p.y-1 && f.x==p.x+2)||(f.y==p.y-1 &&
    f.x==p.x-2)||(f.y==p.y-2 && f.x==p.x+1)||(f.y==p.y-2 && f.x==p.x-1)){a = 1;}
    return a;
}

int Pawn(Position pos, Coordonnees p, Coordonnees f)
{
    int a=1;
    if(pos.turn==BLACK)
    {
        if(p.y==2)
        {
            if(f.y==(p.y+2) && f.x==p.x)
            {
                if(pos.board[f.x][f.y+1].piece.pieceType != NONE || pos.board[p.x][p.y+2].piece.pieceType != NONE)
                {
                    a = 0;
                }
            }
            else if (f.y==(p.y+1) && f.x==p.x)
            {
                if(pos.board[f.x][f.y+1].piece.pieceType != NONE)
                {
                    a = 0;
                }
            }
        }
        else if(f.y==p.y+1 && f.x==p.x)
        {
            if(pos.board[f.x][f.y+1].piece.pieceType!=NONE)
            {
                a = 0;
            }
        }
        else if ( f.y==(p.y+1) && (f.x==(p.x+1)||f.x==(p.x-1)) )
        {
            if(pos.board[f.x][f.y+1].piece.pieceType==NONE)
            {
                a = 0;
            }
        }
    }
    else if(pos.turn==WHITE)
    {
        if(p.y==7)
        {
            if( f.y==(p.y-2) && f.x==p.x)
            {
                if( (pos.board[f.x][f.y+1].piece.pieceType!=NONE)||(pos.board[p.x][p.y].piece.pieceType!=NONE))
                {
                    a = 0;
                }
            }
            else if (f.y==(p.y-1) && f.x==p.x)
            {
                if(pos.board[f.x][f.y+1].piece.pieceType != NONE)
                {
                    a = 0;
                }
            }
        }
        else if(f.y==(p.y-1) && f.x==p.x)
        {
            if(pos.board[f.x][f.y+1].piece.pieceType!=NONE){a = 0;}
        }
        else if(f.y==(p.y-1) && (f.x==(p.x-1)||f.x==(p.x+1)))
        {
            if((pos.board[f.x][f.y+1].piece.pieceType!=NONE)&&(pos.board[p.x][p.y].piece.pieceType!=NONE) ){a = 0;}
        }
    }
    if(a==1)
    {
        if(pos.turn==WHITE)
        {
            if(f.y==8)
                return 2;
        }
        else
        {
            if(f.y==1)
                return 2;
        }
    }
    return a;
}

int canMove(Position pos, Coordonnees p, Coordonnees f)
{
	int a = 0, i;
	int distX = abs(p.x-f.x);
	int distY = abs(p.y-f.y);

    if (pos.board[f.x][f.y+1].piece.pieceType != NONE && pos.board[f.x][f.y+1].piece.color == pos.turn)
        return 0;
    else
    {
        switch (pos.board[p.x][p.y+1].piece.pieceType)
        {
            case KING :
                King(pos, p, f);
                break;

            case QUEEN :
                Queen(pos, p, f);
                break;

            case ROOK :
                Rook(pos, p, f);
                break;

            case BISHOP :
                Bishop(pos, p, f);
                break;

            case KNIGHT :
                Knight(pos, p, f);
                break;

            case PAWN :
                Pawn(pos, p, f);
                break;
            case(NONE):
                break;
        }
    }
}
