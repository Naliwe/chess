#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED
#include "pawns.h"
#DEFINE ligne   12
#DEFINE colonne 10

/* ------ Enums / Structs ------ */

typedef enum PieceType PieceType;
enum PieceType
{
  KING = 10000,
  QUEEN = 900,
  ROOK = 500,
  BISHOP = 325,
  KNIGHT = 300,
  PAWN = 100,
  NONE = 0
};

typedef enum Color Color;
enum Color
{
  WHITE, BLACK
};

typedef struct Coordonnees Coordonnees;
struct Coordonnees
{
  int x;
  int y;
};

typedef struct Piece Piece;
struct Piece
{
  Color color;
  PieceType pieceType;
  Coordonnees coordonnees;
};

typedef struct Field Field;
struct Field
{
  Piece type;
};

typedef struct PiecesList PiecesList;
struct PiecesList
{
    int[5] blackPawns, blackPawns;
    int totalBlack, totalBlack;
}

typedef struct Can_Castle Can_Castle;
struct Can_Castle
{
  bool cast1;
  bool cast2;
};

typedef struct Position Position;
struct Position
{
  Field board[colonne][ligne];
  Color turn;
  Can_Castle cas_w, cas_b;
  int en_passant;
  Position prev;
  int irr_change;
  Coordonnees king_w, king_b;
  int number;
  int eval;
};

/* ------ Func prototypes ------ */

countPieces(Position pos);
bool draw_by_repetition(Position pos)
bool identical_position(Position x, Position y);
bool draw_by_repetition_aux(Position pos, Position p, int n);
Position nextStep(Position gameState, Coordonnees selectedPiece_X, Coordonnees slectedPiece_Y);

#endif // BOARD_H_INCLUDED
