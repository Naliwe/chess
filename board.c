#include <stdio.h>​
#include <stdlib.h>​
#include "board.h"

bool identical_position(Position x, Position y)
{
  return x.eval == y.eval &&
    x.turn == y.turn &&
    x.cas_w == y.cas_w && x.cas_b == y.cas_b &&
    x.en_passant == y.en_passant &&
    x.board == y.board;
}

bool draw_by_repetition_aux(Position pos, Position p, int n)
{
  if (n<=0) true else if (2*n > x.irr_change) false else if (identical_position(p.prev,p)) false else if (identical_position(p,pos) draw_by_repetition_aux(pos,p.prev,n-1) else draw_by_repetition_aux(pos,p.prev,n);
}

PiecesList countPieces(Position pos)
{
    PiecesList numPieces;

    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 12; j++)
        {
            if(pos.board[i][j].type.color == WHITE)
            {
                switch (pos.board[i][j].type.pieceType)
                {
                    case KING:
                        numPieces.whitePawns[0] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case QUEEN:
                        numPieces.whitePawns[1] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case ROOK:
                        numPieces.whitePawns[2] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case BISHOP:
                        numPieces.whitePawns[3] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case KNIGHT:
                        numPieces.whitePawns[4] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case PAWN:
                        numPieces.whitePawns[5] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (pos.board[i][j].type.pieceType)
                {
                    case KING:
                        numPieces.blackPawns[0] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case QUEEN:
                        numPieces.blackPawns[1] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case ROOK:
                        numPieces.blackPawns[2] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case BISHOP:
                        numPieces.blackPawns[3] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case KNIGHT:
                        numPieces.blackPawns[4] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case PAWN:
                        numPieces.blackPawns[5] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
