#ifndef RULES_H_INCLUDED
#define RULES_H_INCLUDED

int King(Position pos, Coordonnees p, Coordonnees f);
int Queen(Position pos, Coordonnees p, Coordonnees f);
int Rook(Position pos, Coordonnees p, Coordonnees f);
int Bishop(Position pos, Coordonnees p, Coordonnees f);
int Knight(Position pos, Coordonnees p, Coordonnees f);
int Pawn(Position pos, Coordonnees p, Coordonnees f);
int canMove(Position pos, Coordonnees p, Coordonnees f);

#endif // RULES_H_INCLUDED
