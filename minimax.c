#include "minimax.h"
#include "pawns.h"
#include "chess.h"

Position AlphaBeta(Position pos,                 /*-> game state : undefined for now ( board ? ). Should represent a node in the Minimax tree */
              int depth,
		      int alpha, int beta)
{
    int pos.eval, pos.noteForNow, i;              /* pos.eval -> best move found. pos.noteForNow -> best move found SO FAR. */

    if (depth == 0)                           /* If the game ends or if max depth is reached */
        return eval(pos);                     // Return the current move

    if (pos.turn == WHITE)
        {
            pos.eval = alpha;

            for (i = 1; i < 10; i++)
            {
                for (j = 2; i < 12; j++)
                {
                    Coordonnees dep;
                    dep.x = i;
                    dep.y = j;
                    pos.noteForNow = AlphaBeta(jouerCoup(pos, dep.x, dep.y),  // Recursive call to determine wtf on other nodes (further possible moves)
                                                                   depth - 1,
                                    pos.eval,
                                    beta);
                    if (pos.noteForNow > pos.eval)            //If current note is better than the highest, set the highest to the current (Captain to the rescue !)
                        pos.eval = pos.noteForNow;
                    if (pos.eval >= beta)                 //Do not check other nodes on that branch, just prune it, you already found the best move you could have
                        break ;
                }
            }
        }
    /* BLACK : Same shit as up there, but if it's black turn */
    else
        {
            pos.eval = beta;

            for (i = 1; i < 10; i++)
            {
                for (j = 2; i < 12; j++)
                {
                    Coordonnees dep;
                    dep.x = i;
                    dep.y = j;
                    pos.noteForNow = AlphaBeta(jouerCoup(pos, dep.x, dep.y),  // Recursive call to determine wtf on other nodes (further possible moves)
                                    WHITE,
                                    depth - 1,
                                    pos.eval,
                                    beta);
                    if (pos.noteForNow < pos.eval)            //If current note is better than the highest, set the highest to the current (Captain to the rescue !)
                        pos.eval = pos.noteForNow;
                    if (pos.eval <= beta)                 //Do not check other nodes on that branch, just prune it, you already found the best move you could have
                        break ;
                }
            }
        }
    return eval(pos);
}

int eval(Position pos)
{
    PiecesList list = countPieces(pos);
    int score;

    if (pos.turn == BLACK)
    {
        score = list.blackPawns[0]       +
                list.blackPawns[1] * 900 +
                list.blackPawns[2] * 500 +
                list.blackPawns[3] * 325 +
                list.blackPawns[4] * 300 +
                list.blackPawns[5] * 100;
    }

    else
    {
        score = list.whitePawns[0]       +
                list.whitePawns[1] * 900 +
                list.whitePawns[2] * 500 +
                list.whitePawns[3] * 325 +
                list.whitePawns[4] * 300 +
                list.whitePawns[5] * 100;
    }

    return score;
}
Position nextStep(Position gameState, Coordonnees selectedPiece_X, Coordonnees slectedPiece_Y)
{
    AlphaBeta(gameState, DEPTH, -15000, 15000);
}

