#include <stdio.h>​
#include <stdlib.h>​

#include "chess.h"
#include "rules.h"

#define ligne 12
#define colonne 10

//Il manque la gestion des echecs, les en passant, la sauvegarde.

Position gameBoard;

int identical_position(Position x, Position y)
{
  return (x.eval == y.eval &&
    x.turn == y.turn &&
    x.cas_w.cast1 == y.cas_w.cast1 && x.cas_w.cast2 == y.cas_w.cast2 &&
    x.cas_b.cast1 == y.cas_b.cast1 && x.cas_b.cast2 == y.cas_w.cast2 &&
    x.en_passant == y.en_passant &&
    x.board == y.board);
}

int draw_by_repetition_aux(Position pos, Position p, int n)
{
  if (n<=0)
  {
    return 1;
  }
    else if (2*n > p.irr_change)
    {
        return 0;
    }
    else if (identical_position(*(p.prev),p))
    {
    return 0;
    }
    else if (identical_position(p,pos))
    {
    draw_by_repetition_aux(pos,*(p.prev),n-1);
    }
    else
    {
    draw_by_repetition_aux(pos,*(p.prev),n);
    }
}

void init()
{
    int i,j;
	for(j=0;j<ligne;j++)
	{
	    if (j<= 1 || j >= ligne-2)
	    {
            for(i=0;i<colonne;i++)
            {
                gameBoard.board[i][j].piece.color=BLACK;
                gameBoard.board[i][j].piece.pieceType=NONE;
                gameBoard.board[i][j].piece.coords.x=i;
                gameBoard.board[i][j].piece.coords.y=j-1;
            }
	    }
	    else
	    {
                gameBoard.board[0][j].piece.color=BLACK;
                gameBoard.board[0][j].piece.pieceType=NONE;
                gameBoard.board[0][j].piece.coords.x=0;
                gameBoard.board[0][j].piece.coords.y=j-1;
                gameBoard.board[colonne-1][j].piece.color=BLACK;
                gameBoard.board[colonne-1][j].piece.pieceType=NONE;
                gameBoard.board[colonne-1][j].piece.coords.x=colonne-1;
                gameBoard.board[colonne-1][j].piece.coords.y=j-1;

                if (j == 3)
                {
                    for(i=1;i<colonne-1;i++)
                    {
                        gameBoard.board[i][j].piece.color=BLACK;
                        gameBoard.board[i][j].piece.pieceType=PAWN;
                        gameBoard.board[i][j].piece.coords.x=i;
                        gameBoard.board[i][j].piece.coords.y=j-1;
                    }
                }
                else if (j == 8)
                {
                    for(i=1;i<colonne-1;i++)
                    {
                        gameBoard.board[i][j].piece.color=WHITE;
                        gameBoard.board[i][j].piece.pieceType=PAWN;
                        gameBoard.board[i][j].piece.coords.x=i;
                        gameBoard.board[i][j].piece.coords.y=j-1;
                    }
                }
                else if(j>3 && j<6)
                {
                    for(i=1;i<colonne-1;i++)
                    {
                        gameBoard.board[i][j].piece.color=WHITE;
                        gameBoard.board[i][j].piece.pieceType=NONE;
                        gameBoard.board[i][j].piece.coords.x=i;
                        gameBoard.board[i][j].piece.coords.y=j-1;
                    }
                }

	    }
	}

	gameBoard.board[1][2].piece.color = BLACK;
    gameBoard.board[1][2].piece.pieceType = ROOK;
    gameBoard.board[1][2].piece.coords.x = 1;
    gameBoard.board[1][2].piece.coords.y = 1;

    gameBoard.board[2][2].piece.color = BLACK;
    gameBoard.board[2][2].piece.pieceType = KNIGHT;
    gameBoard.board[2][2].piece.coords.x = 2;
    gameBoard.board[2][2].piece.coords.y = 1;

	gameBoard.board[3][2].piece.color = BLACK;
    gameBoard.board[3][2].piece.pieceType = BISHOP;
    gameBoard.board[3][2].piece.coords.x = 3;
    gameBoard.board[3][2].piece.coords.y = 1;

	gameBoard.board[4][2].piece.color = BLACK;
    gameBoard.board[4][2].piece.pieceType = QUEEN;
    gameBoard.board[4][2].piece.coords.x = 4;
    gameBoard.board[4][2].piece.coords.y = 1;

	gameBoard.board[5][2].piece.color = BLACK;
    gameBoard.board[5][2].piece.pieceType = KING;
    gameBoard.board[5][2].piece.coords.x = 5;
    gameBoard.board[5][2].piece.coords.y = 1;

	gameBoard.board[6][2].piece.color = BLACK;
    gameBoard.board[6][2].piece.pieceType = BISHOP;
    gameBoard.board[6][2].piece.coords.x = 6;
    gameBoard.board[6][2].piece.coords.y = 1;

	gameBoard.board[7][2].piece.color = BLACK;
    gameBoard.board[7][2].piece.pieceType = KNIGHT;
    gameBoard.board[7][2].piece.coords.x = 7;
    gameBoard.board[7][2].piece.coords.y = 1;

	gameBoard.board[8][2].piece.color = BLACK;
    gameBoard.board[8][2].piece.pieceType = ROOK;
    gameBoard.board[8][2].piece.coords.x = 8;
    gameBoard.board[8][2].piece.coords.y = 1;



	gameBoard.board[1][9].piece.color = WHITE;
    gameBoard.board[1][9].piece.pieceType = ROOK;
    gameBoard.board[1][9].piece.coords.x = 1;
    gameBoard.board[1][9].piece.coords.y = 8;

    gameBoard.board[2][9].piece.color = WHITE;
    gameBoard.board[2][9].piece.pieceType = KNIGHT;
    gameBoard.board[2][9].piece.coords.x = 2;
    gameBoard.board[2][9].piece.coords.y = 8;

	gameBoard.board[3][9].piece.color = WHITE;
    gameBoard.board[3][9].piece.pieceType = BISHOP;
    gameBoard.board[3][9].piece.coords.x = 3;
    gameBoard.board[3][9].piece.coords.y = 8;

	gameBoard.board[4][9].piece.color = WHITE;
    gameBoard.board[4][9].piece.pieceType = QUEEN;
    gameBoard.board[4][9].piece.coords.x = 4;
    gameBoard.board[4][9].piece.coords.y = 8;

	gameBoard.board[5][9].piece.color = WHITE;
    gameBoard.board[5][9].piece.pieceType = KING;
    gameBoard.board[5][9].piece.coords.x = 5;
    gameBoard.board[5][9].piece.coords.y = 8;

	gameBoard.board[6][9].piece.color = WHITE;
    gameBoard.board[6][9].piece.pieceType = BISHOP;
    gameBoard.board[6][9].piece.coords.x = 6;
    gameBoard.board[6][9].piece.coords.y = 8;

	gameBoard.board[7][9].piece.color = WHITE;
    gameBoard.board[7][9].piece.pieceType = KNIGHT;
    gameBoard.board[7][9].piece.coords.x = 7;
    gameBoard.board[7][9].piece.coords.y = 8;

	gameBoard.board[8][9].piece.color = WHITE;
    gameBoard.board[8][9].piece.pieceType = ROOK;
    gameBoard.board[8][9].piece.coords.x = 8;
    gameBoard.board[8][9].piece.coords.y = 8;


	gameBoard.turn = WHITE;
    gameBoard.cas_b.cast1 = 0;
    gameBoard.cas_b.cast2 = 0;
    gameBoard.cas_w.cast1 = 0;
    gameBoard.cas_w.cast2 = 0;
    gameBoard.en_passant = 0;
    gameBoard.prev = NULL;
    gameBoard.irr_change = 0;
    gameBoard.king_b.x = 5;
    gameBoard.king_b.y = 1;
    gameBoard.king_w.x = 4;
    gameBoard.king_w.y = 8;
    gameBoard.number = 0;
    gameBoard.eval = 0;
}

void drawBoard()
{
int a, b, c=0, couleur;

	system ("clear");		/*efface l'ecran*/
	printf("\n\t.________________________________________________________.\n\t");
    printf("||      |      |      |      |      |      |      |      |\n\t");
    printf("||  A   |  B   |  C   |  D   |  E   |  F   |  G   |  H   |\n\t");
	printf("||______|______|______|______|______|______|______|______|\n\t");
	for (a=2;a<10;a++)
	{
	    printf("||      |      |      |      |      |      |      |      |\n\t|");
		for (b=1;b<9;b++)
		{
			c++;
			printf("|  %c%c  ",ccouleur(gameBoard.board[b][a]),ctype(gameBoard.board[b][a]));
			if ( c % 8 == 0)
			printf("| %d\n\t||______|______|______|______|______|______|______|______|\n\t", c/8);
		}
	}
    printf("||      |      |      |      |      |      |      |      |\n\t");
    printf("||  A   |  B   |  C   |  D   |  E   |  F   |  G   |  H   |\n\t");
	printf("||______|______|______|______|______|______|______|______|\n\t");
}

Position jouerCoup(Position dep, Coordonnees depart, Coordonnees arrivee)
{
    Piece piece_vide;
    piece_vide.color = WHITE;
    piece_vide.pieceType = NONE;
    piece_vide.coords = depart;

    Position end = dep;
    end.board[arrivee.x][arrivee.y+1].piece = end.board[depart.x][depart.y+1].piece;
    end.board[arrivee.x][arrivee.y+1].piece.coords = arrivee;
    end.board[depart.x][depart.y+1].piece = piece_vide;
    return end;
}

char ccouleur(Field f)
{
    if(f.piece.pieceType != NONE)
    {
        switch(f.piece.color)
        {
            case(BLACK):
                return 'B';
            case(WHITE):
                return 'W';
        }
    }
    else return ' ';
}

char ctype(Field f)
{
        switch(f.piece.pieceType)
        {
            case(NONE):
                return ' ';
            case(KING):
                return 'K';
            case(QUEEN):
                return 'q';
            case(ROOK):
                return 'r';
            case(BISHOP):
                return 'b';
            case(KNIGHT):
                return 'k';
            case(PAWN):
                return 'p';
        }
}

char column_of_int(int n)
{
    switch(n)
    {
        case(1):
            return 'A';
        case(2):
            return 'B';
        case(3):
            return 'C';
        case(4):
            return 'D';
        case(5):
            return 'E';
        case(6):
            return 'F';
        case(7):
            return 'G';
        case(8):
            return 'H';
    }
}

int int_of_column(char c)
{
    switch(c)
    {
        case('a'):
        case('A'):
            return 1;
        case('b'):
        case('B'):
            return 2;
        case('c'):
        case('C'):
            return 3;
        case('d'):
        case('D'):
            return 4;
        case('e'):
        case('E'):
            return 5;
        case('f'):
        case('F'):
            return 6;
        case('g'):
        case('G'):
            return 7;
        case('h'):
        case('H'):
            return 8;
    }
}

void newGame()
{
    int mate = 0;
    char from[2], to[2];
    Coordonnees fromm, too;

    init();
    while(!mate)
    {
        drawBoard();
        printf("\nWhich piece to move?\n");
        scanf("%s", &from);
        fromm.x = int_of_column(from[0]);
        fromm.y = from[1]-48;
        //printf("%c%c", fromm.x + 64, fromm.y + 48);
        if (fromm.x > 0 && fromm.x < 9 && fromm.y > 0 && fromm.y < 9 && gameBoard.board[fromm.x][fromm.y+1].piece.pieceType != NONE && gameBoard.board[fromm.x][fromm.y+1].piece.color == gameBoard.turn)
        {
            printf("Where do you want to move it?\n");
            scanf("%s", &to);
            too.x = int_of_column(to[0]);
            too.y = to[1]-48;
            //printf("%c%c\n", too.x + 64, too.y+48);
            if (too.x > 0 && too.x < 9 && too.y > 0 && too.y < 9 && (gameBoard.board[too.x][too.y+1].piece.pieceType == NONE || gameBoard.board[too.x][too.y+1].piece.color != gameBoard.turn))
            {
                if (canMove(gameBoard, fromm, too))
                {
                    gameBoard = jouerCoup(gameBoard, fromm, too);
                    gameBoard.turn = !gameBoard.turn;
                }
                else
                {
                    printf("Forbidden move!\n");
                }
            }
            else
            {
                printf("The destination entered isn't valid.\n");
            }
        }
        else
        {
            printf("There is none of your pieces at this location.\n");
        }
    }
}

PiecesList countPieces(Position pos)
{
    int i, j;
    PiecesList numPieces;

    for (i = 0; i < colonne; i++)
    {
        for (j = 0; j < ligne; j++)
        {
            if(pos.board[i][j].piece.color == WHITE)
            {
                switch (pos.board[i][j].piece.pieceType)
                {
                    case KING:
                        numPieces.whitePawns[0] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case QUEEN:
                        numPieces.whitePawns[1] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case ROOK:
                        numPieces.whitePawns[2] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case BISHOP:
                        numPieces.whitePawns[3] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case KNIGHT:
                        numPieces.whitePawns[4] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    case PAWN:
                        numPieces.whitePawns[5] += 1;
                        numPieces.totalWhite += 1;
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (pos.board[i][j].piece.pieceType)
                {
                    case KING:
                        numPieces.blackPawns[0] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case QUEEN:
                        numPieces.blackPawns[1] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case ROOK:
                        numPieces.blackPawns[2] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case BISHOP:
                        numPieces.blackPawns[3] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case KNIGHT:
                        numPieces.blackPawns[4] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    case PAWN:
                        numPieces.blackPawns[5] += 1;
                        numPieces.totalBlack += 1;
                        break;

                    default:
                        break;
                }
            }
        }
    }
}

int main()
{

int choix=-1, test=0;
while(choix != 0)
{
    				/*efface l'ecran*/
	printf("Welcome to Against the Machine\n");
	printf("1. New Game\n");
	// printf("2. Load\n");
	// printf("3. Credit\n");
	printf("0. Quit\n");
	printf("Pick a choice ");
	scanf("%d",&choix);

	switch (choix)
	{
		case 1 :
			newGame();
			break;
        // case 2 :chargement();
		//	break;
        // case 3 :credit();
		case 0 :printf("Goodbye\n");
			break;
		default :printf("Unrecognized entry\n");
			 break;
	}
}
}

